import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import avatarImg from './../../assets/avatar.jpg'
import './Header.scss'
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import { Link as RouteLink } from "react-router-dom"
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <AppBar position="sticky">
        <Paper className="header__paper">
          <Container maxWidth="lg">
            <Toolbar
              className="header"
              variant="regular"
            >
              <div className="header__user">
                <Avatar src={avatarImg} />
                <div className="header__desc">
                  <Typography variant="h6">
                    Иванов Степан
                  </Typography>
                  <Typography variant="subtitle1">
                    frontend-разработчик
                  </Typography>
                </div>
              </div>
              <nav className="header__nav">
                <Link
                  color="inherit"
                  className="header__nav-item"
                >
                  <RouteLink to={'/'}>
                    ПОРТФОЛИО
                  </RouteLink>
                </Link>
                <Link
                  color="inherit"
                  className="header__nav-item"
                >
                  <RouteLink
                    to={'/tutorial'}
                  >
                    ОБУЧЕНИЕ
                  </RouteLink>
                </Link>
                <Link
                  color="inherit"
                  onClick={handleOpen}
                  className="header__nav-item"
                >
                  КОНТАКТЫ
                </Link>
              </nav>
            </Toolbar>
          </Container>
        </Paper>
      </AppBar>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Написать, позвонить</h2>
            <p>
              Телефон&nbsp;&nbsp;
              <a
                className="link"
                href="tel:+79501464445"
              >
                +79501464445
              </a>
              <span>&nbsp;(WhatsApp, Viber)</span>
            </p>
            <p>
              Почта&nbsp;&nbsp;
              <a
                className="link"
                href="mailto:stepan.ivanov93@gmail.com"
              >
                stepan.ivanov93@gmail.com
              </a>
            </p>
            <p>
              Telegram&nbsp;&nbsp;
              <a
                className="link"
                href="https://telegram.me/ste_iv"
              >
                ste_iv
              </a>
            </p>
            <p>
              Skype&nbsp;&nbsp;
              <a
                className="link"
                href="skype:live:stepan.ivanov93_1?chat">live:stepan.ivanov93_1</a>
            </p>
            <Divider />
            <p>
              <a
                target="_blank"
                className="link"
                href="https://hh.ru/resume/3a21b49aff03eb06640039ed1f5247434f4f65"
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <OpenInNewIcon />
                Резюме
              </a>
            </p>
          </div>
        </Fade>
      </Modal>
    </>
  )
};