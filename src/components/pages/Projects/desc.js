import nagradaImg from '../../../assets/nagrada.jpg';
import newSmileImg from '../../../assets/site-newsmile2.jpg';
import erpImg from '../../../assets/erp-tablet.png';
import wfImg from '../../../assets/wf.png';
import sweetsImg from '../../../assets/sweets.jpg';
import ntpcImg from '../../../assets/ntpc.jpg';
import beautyImg from '../../../assets/beauty saloon.jpg';
import winsImg from '../../../assets/win-products.jpg';
import prtImg from '../../../assets/pritok.jpg_1552473202';

export const desc = [
  {
    id: 1,
    desc: {
      title: 'New Smile',
      text: 'Сайт сети стоматологических клиник New Smile',
      img: newSmileImg
    },
    link: {
      url: 'https://newsmile.clinic/',
      text: 'newsmile.clinic'
    }
  },
  {
    id: 2,
    desc: {
      title: 'ERP NewSmile',
      text: 'Онлайн-запись и автоматизация процессов стоматологии',
      img: erpImg
    },
    link: {
      url: 'http://erp.newsmile.clinic/',
      text: 'erp.newsmile.clinic'
    }
  },
  {
    id: 3,
    desc: {
      title: 'Приток веб',
      text: 'ERP Охранных систем (в разработке)',
      img: prtImg
    },
    link: {
      url: 'https://www.sokrat.ru/',
      text: 'sokrat.ru'
    }
  },
  {
    id: 3,
    desc: {
      title: 'Награда',
      text: 'Известный производитель наградной, сувенирной, текстильной продукции',
      img: nagradaImg
    },
    link: {
      url: 'https://nagrada38.ru/',
      text: 'nagrada38.ru'
    }
  },
  {
    id: 4,
    desc: {
      title: 'Wifire',
      text: 'Интернет провайдер – Wifire',
      img: wfImg
    },
    link: {
      url: 'https://www.wifire.ru/wifirevas',
      text: 'wifire.ru'
    }
  },
  {
    id: 5,
    desc: {
      title: 'Сладости38',
      text: 'Кондитерские изделия оптом',
      img: sweetsImg
    },
    link: {
      url: 'https://sladosti38.ru/',
      text: 'sladosti38.ru'
    }
  },
  {
    id: 6,
    desc: {
      title: 'НТПЦ',
      text: 'Ведущий поставщик информационных решений и услуг на рынке России',
      img: ntpcImg
    },
    link: {
      url: 'https://ntpc.ru/',
      text: 'ntpc.ru'
    }
  },
  {
    id: 7,
    desc: {
      title: 'Перманентный макияж',
      text: 'Обучение перманентному макияжу с нуля',
      img: beautyImg
    },
    link: {
      url: 'http://obuchenie.nopina.ru/',
      text: 'obuchenie.nopina.ru'
    }
  },
  {
    id: 8,
    desc: {
      title: 'Нопико',
      text: 'Наградная продукция',
      img: winsImg
    },
    link: {
      url: 'https://xn--h1aehecd.xn--p1ai/',
      text: 'нопико.рф'
    }
  },
];