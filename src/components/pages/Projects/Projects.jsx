import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Container from '@material-ui/core/Container';
import Case from '../../Case/Case';

import newSmileImg from './../../../assets/site-newsmile2.jpg'
import nagradaImg from './../../../assets/nagrada.jpg'
import { desc } from './desc';


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 315,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  wrapper: {
    flexGrow: 1,
    maxWidth: 1140,
    margin: '30px auto'
  },
  buttonPanel: {
    // justifyContent: 'space-between'
    justifyContent: 'center'
  }
}));

function CaseList() {
  return (
    <Grid container spacing={3}>
      {desc.map(i => (
          <Grid item xs={4}>
            <Case
              key={i.id}
              desc={{
                title: i.desc.title,
                text: i.desc.text,
                img: i.desc.img
              }}
               link={{
                url: i.link.url,
                text: i.link.text
              }}
            />
          </Grid>
        )
      )}
    </Grid>
  )
}

export default function ImgMediaCard() {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Container maxWidth="lg">
        <CaseList />
      </Container>
    </div>
  );
}
