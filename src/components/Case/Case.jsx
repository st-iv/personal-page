import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import newSmileImg from './../../assets/site-newsmile2.jpg'
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Container from '@material-ui/core/Container';
import './Case.scss';

import nagradaImg from './../../assets/nagrada.jpg'


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 315,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  wrapper: {
    flexGrow: 1,
    maxWidth: 1140,
    margin: '30px auto'
  },
  buttonPanel: {
    // justifyContent: 'space-between'
    justifyContent: 'center'
  }
}));

export default function Case(props) {
  const { desc, link } = props;
  const classes = useStyles();

    return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="140"
            image={desc.img}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {desc.title}
            </Typography>
            <Typography
              className="case__text"
              variant="body2" color="textSecondary" component="p"
            >
              {desc.text}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions className={classes.buttonPanel}>
          {/*<Button
            size="small"
          >
            подробнее
          </Button>*/}
          <Button
            size="small"
            color="secondary"
            href={link.url}
            target="_blank"
          >
            <OpenInNewIcon />
            {link.text}
          </Button>
        </CardActions>
      </Card>
    );
}