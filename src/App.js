import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header/Header';
import Projects from './components/pages/Projects/Projects';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { grey, lime, } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';
import {
  Route,
  Switch,
  Redirect,
  withRouter
} from "react-router-dom"
import Tutorial from './components/pages/Tutorial/Tutorial';



const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: grey[500]
    },
    secondary: {
      main: lime[500]
    }
  },
});

function App(props) {
  const { history } = props;
  return (
    <div className="App">
      <ThemeProvider
        theme={theme}
      >
        <CssBaseline />
        <Header />
        <Switch>
          <Route history={history} path='/' component={Projects} exact />
          <Route history={history} path='/tutorial' component={Tutorial} exact/>
          <Redirect from='/' to='/'/>
        </Switch>
      </ThemeProvider>
    </div>
  );
}

export default App;
